package ru.tsc.tambovtsev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.model.IRepository;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import java.util.Collection;

public interface IService<M extends AbstractEntity> extends IRepository<M> {

    @Nullable
    M findById(@Nullable String id);

    void removeById(@Nullable String id);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void create(@NotNull M model);

    void update(@NotNull M model);

}

