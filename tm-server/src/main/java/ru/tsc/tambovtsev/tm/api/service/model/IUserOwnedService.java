package ru.tsc.tambovtsev.tm.api.service.model;

import ru.tsc.tambovtsev.tm.api.repository.model.IOwnerRepository;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IOwnerRepository<M>, IService<M> {
}
